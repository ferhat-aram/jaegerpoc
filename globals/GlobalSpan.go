package globals

import (
	"fmt"
	"github.com/opentracing/opentracing-go"
)

var singletonGlobalSpan *globalSpan

func GetGlobalSpan() *globalSpan {
	if singletonGlobalSpan == nil {
		singletonGlobalSpan = &globalSpan{}
	}
	return singletonGlobalSpan
}

type globalSpan struct {
	spans []opentracing.Span
}

func (g *globalSpan) Push(span opentracing.Span) {
	g.spans = append(g.spans, span)
}

func (g *globalSpan) Pop() error {
	if len(g.spans) > 0 {
		defer g.spans[len(g.spans)-1].Finish()
		g.spans = g.spans[:len(g.spans)-1]
		return nil
	}
	return fmt.Errorf("can't pop non existent element out of empty stack")
}

func (g *globalSpan) Top() (opentracing.Span, error) {
	if len(g.spans) > 0 {
		return g.spans[len(g.spans)-1], nil
	}
	return nil, fmt.Errorf("can't reference to non existent top element of empty stack")
}

func StartSpan(operationName string, opts ...opentracing.StartSpanOption) {
	GetGlobalSpan().Push(opentracing.GlobalTracer().StartSpan(operationName, opts ...))
}

func Finish() error {
	err := GetGlobalSpan().Pop()
	if err != nil {
		return err
	}
	return nil
}

/*
	Functionality is given so far, but problems would arise, if we are going to use this small library in multithreaded applications.
 */