package main

import (
	"fmt"
	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go"
	jaegercfg "github.com/uber/jaeger-client-go/config"
	"jaegerPlayground/globals"
	"log"
	"time"
)

type Dog struct {}

func (d *Dog) bark() error {
	parentSpan, err := globals.GetGlobalSpan().Top()
	if err != nil {
		return err
	}
	globals.StartSpan("Bark", opentracing.ChildOf(parentSpan.Context()))
	time.Sleep(2 * time.Second)
	defer globals.Finish()
	fmt.Println("Bark! Bark!")
	return nil
}

func (d *Dog) run() error {
	parentSpan, err := globals.GetGlobalSpan().Top()
	if err != nil {
		return err
	}
	globals.StartSpan("Run", opentracing.ChildOf(parentSpan.Context()))
	time.Sleep(2 * time.Second)
	defer globals.Finish()
	fmt.Println("Run! Run! Run!")
	return nil
}

func (d *Dog) sleep() error {
	parentSpan, err := globals.GetGlobalSpan().Top()
	if err != nil {
		return err
	}
	globals.StartSpan("Sleep", opentracing.ChildOf(parentSpan.Context()))
	time.Sleep(2 * time.Second)
	defer globals.Finish()
	fmt.Println("...Zzzzzzzzz....")
	return nil
}

func main() {
	// Sample configuration for testing. Use constant sampling to sample every trace
	// and enable LogSpan to log every span via configured Logger.

	cfg := jaegercfg.Configuration{
		Sampler: &jaegercfg.SamplerConfig{
			Type:  jaeger.SamplerTypeConst,
			Param: 1,

		},
		Reporter: &jaegercfg.ReporterConfig{
			LogSpans: true,
			LocalAgentHostPort: "nue-jelk-01.paesslergmbh.de:6831",
		},
	}

	// Initialize tracer with a logger and a metrics factory
	closer, err := cfg.InitGlobalTracer(
		"Test",
	)
	if err != nil {
		log.Printf("Could not initialize jaeger tracer: %s", err.Error())
		return
	}
	defer closer.Close()
	globals.StartSpan("Test")
	defer globals.Finish()
	time.Sleep(2 * time.Second)
	dog := Dog{}
	err = dog.bark()
	if err != nil {
		panic(err)
	}
	err = dog.run()
	if err != nil {
		panic(err)
	}
	err = dog.sleep()
	if err != nil {
		panic(err)
	}
}

